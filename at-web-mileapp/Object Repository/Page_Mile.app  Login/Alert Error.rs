<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Alert Error</name>
   <tag></tag>
   <elementGuidId>cb853a1e-bd09-4ce5-9a02-3c2975641c77</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>el-alert--error</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>.el-alert--error</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>504b402e-7dd1-482a-a7d5-47761323eb67</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>el-alert__icon el-icon-error is-big</value>
      <webElementGuid>ea8134be-b633-4c0b-9e83-e2e5cf3115e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;login_page&quot;]/div[@class=&quot;bg&quot;]/div[@class=&quot;container login_container&quot;]/div[@class=&quot;inner_content el-row is-justify-left&quot;]/div[@class=&quot;el-col el-col-24 el-col-xs-24 el-col-sm-24 el-col-md-8 el-col-lg-8 el-col-xl-8&quot;]/div[@class=&quot;el-row&quot;]/div[@class=&quot;el-col el-col-24 el-col-xs-24 el-col-sm-24 el-col-md-24 el-col-lg-24 el-col-xl-24&quot;]/div[1]/div[@class=&quot;el-alert el-alert--error is-light&quot;]/i[@class=&quot;el-alert__icon el-icon-error is-big&quot;]</value>
      <webElementGuid>0e5204e1-e296-4abf-9c4e-28ae5c73d984</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[2]/div/div/div/div/div/div/div/i</value>
      <webElementGuid>193d6a51-ec27-4959-b978-925d365861ef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/i</value>
      <webElementGuid>b84058db-fab5-4d62-9164-8747e1573dab</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
