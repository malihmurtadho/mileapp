<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Button back to home</name>
   <tag></tag>
   <elementGuidId>9adeb3f2-08c6-4467-9310-cb3437114dbd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;app&quot;]/div[1]/div[2]/div/div[1]/div[1]/a[1]/text()</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>.back_home</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
