import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://taskdev.mile.app/login')

WebUI.verifyElementVisible(findTestObject('Page_Mile.app  Login/Button back to home'))

WebUI.verifyElementText(findTestObject('Page_Mile.app  Login/header'), 'Call Us Now: +62 812-1133-5608')

WebUI.verifyElementText(findTestObject('Page_Mile.app  Login/Button back to home'), 'Back to home')

WebUI.verifyElementVisible(findTestObject('Page_Mile.app  Login/logo mileapp'))

WebUI.verifyElementVisible(findTestObject('Page_Mile.app  Login/background login'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('Page_Mile.app  Login/title form'))

WebUI.verifyElementText(findTestObject('Page_Mile.app  Login/title form'), 'Login')

WebUI.verifyElementVisible(findTestObject('Page_Mile.app  Login/label name organization'))

WebUI.verifyElementText(findTestObject('Page_Mile.app  Login/label name organization'), 'Organization Name')

WebUI.verifyElementVisible(findTestObject('Page_Mile.app  Login/field name organization'))

WebUI.verifyElementVisible(findTestObject('Page_Mile.app  Login/label email or username'))

WebUI.verifyElementText(findTestObject('Page_Mile.app  Login/label email or username'), 'Email or Username')

WebUI.verifyElementNotClickable(findTestObject('Page_Mile.app  Login/field email or username'))

WebUI.verifyElementVisible(findTestObject('Page_Mile.app  Login/label password'))

WebUI.verifyElementText(findTestObject('Page_Mile.app  Login/label password'), 'Password')

WebUI.verifyElementNotClickable(findTestObject('Page_Mile.app  Login/field password'))

WebUI.verifyElementNotClickable(findTestObject('Page_Mile.app  Login/button Login'))

WebUI.verifyElementText(findTestObject('Page_Mile.app  Login/button Login'), 'Login')

WebUI.verifyElementText(findTestObject('Page_Mile.app  Login/request demo 1'), 'Not registered yet? Contact us for more info')

WebUI.verifyElementText(findTestObject('Page_Mile.app  Login/footer'), '© Copyright 2021 PT. Paket Informasi Digital. All Rights Reserved')

WebUI.closeBrowser()

